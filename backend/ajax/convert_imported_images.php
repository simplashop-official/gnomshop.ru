<?php

use Okay\Core\Database;
use Okay\Core\EntityFactory;
use Okay\Core\Image;
use Okay\Core\ManagerMenu;
use Okay\Core\Managers;
use Okay\Core\Modules\EntityMigrator;
use Okay\Core\Modules\Extender\ExtenderFacade;
use Okay\Core\Modules\Module;
use Okay\Core\Modules\Modules;
use Okay\Core\Modules\ModulesEntitiesFilters;
use Okay\Core\Modules\UpdateObject;
use Okay\Core\ServiceLocator;
use Okay\Entities\ManagersEntity;
use Okay\Core\QueryFactory;
use Okay\Core\Import;
use Okay\Admin\Helpers\BackendImportHelper;

require_once 'configure.php';

///** @var ManagersEntity $managersEntity */
//$managersEntity = $entityFactory->get(ManagersEntity::class);

/** @var QueryFactory $queryBuilder */
$queryBuilder = $DI->get(QueryFactory::class);

///** @var Import $import */
//$import = $DI->get(Import::class);

///** @var BackendImportHelper $importHelper */
//$importHelper = $DI->get(BackendImportHelper::class);

$sql = $queryBuilder->newSqlQuery()->setStatement("SELECT * FROM __images WHERE filename LIKE 'http%'")->execute();
$results = $sql->results();

$serviceLocator = ServiceLocator::getInstance();
$image = $serviceLocator->getService(Image::class);

foreach ($results as $result) {
    $originFile = $image->downloadImage(rawurldecode($result->filename));
}