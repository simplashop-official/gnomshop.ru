<?php
/* Smarty version 3.1.36, created on 2020-06-16 17:22:49
  from '/home/gnomshop/public_html/backend/design/html/module_design.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee8d5b9542099_63457449',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e5b6ce334da3d08229905f3cff9c2d5964b4479f' => 
    array (
      0 => '/home/gnomshop/public_html/backend/design/html/module_design.tpl',
      1 => 1589884281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:svg_icon.tpl' => 2,
  ),
),false)) {
function content_5ee8d5b9542099_63457449 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('meta_title', $_smarty_tpl->tpl_vars['module']->value->module_name ,false ,32);?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="wrap_heading">
            <div class="box_heading heading_page">
                <?php echo $_smarty_tpl->tpl_vars['btr']->value->module_design_title;?>
 (<?php echo $_smarty_tpl->tpl_vars['module']->value->vendor;?>
/<?php echo $_smarty_tpl->tpl_vars['module']->value->module_name;?>
)
            </div>
        </div>
    </div>
</div>

<div class="alert alert--icon">
    <div class="alert__content">
        <div class="alert__title mb-q"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->alert_description, ENT_QUOTES, 'UTF-8', true);?>
</div>
        <p><?php echo $_smarty_tpl->tpl_vars['btr']->value->theme_coverage_module_design;?>
</p>
    </div>
</div>

<div class="boxed fn_toggle_wrap">
    <?php if ($_smarty_tpl->tpl_vars['files']->value) {?>
        <form class="fn_form_list" method="post">
            <div class="okay_list products_list fn_sort_list">
                <input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">

                                <div class="deliveries_wrap okay_list_body sortable">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['files']->value, 'file');
$_smarty_tpl->tpl_vars['file']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
$_smarty_tpl->tpl_vars['file']->do_else = false;
?>
                        <div class="fn_row okay_list_body_item fn_sort_item">
                            <div class="okay_list_row">
                                                                <div class="okay_list_boding okay_list_check">
                                    <input class="hidden_check" type="checkbox" id="id_<?php echo $_smarty_tpl->tpl_vars['file']->value->directory;
echo $_smarty_tpl->tpl_vars['file']->value->filename;?>
" name="check[]" value="<?php echo $_smarty_tpl->tpl_vars['file']->value->directory;
echo $_smarty_tpl->tpl_vars['file']->value->filename;?>
"/>
                                    <label class="okay_ckeckbox" for="id_<?php echo $_smarty_tpl->tpl_vars['file']->value->directory;
echo $_smarty_tpl->tpl_vars['file']->value->filename;?>
"></label>
                                </div>
                                <div class="okay_list_boding okay_list_module_design_name">
                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value->directory, ENT_QUOTES, 'UTF-8', true);
echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value->filename, ENT_QUOTES, 'UTF-8', true);?>

                                </div>
                                <div class="okay_list_boding okay_list_module_design_status">
                                    <?php if (!$_smarty_tpl->tpl_vars['file']->value->cloned_to_theme) {?>
                                        <button class="btn btn_small btn-outline-warning" name="clone_single_file" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value->directory, ENT_QUOTES, 'UTF-8', true);
echo htmlspecialchars($_smarty_tpl->tpl_vars['file']->value->filename, ENT_QUOTES, 'UTF-8', true);?>
"><?php echo $_smarty_tpl->tpl_vars['btr']->value->module_design_copy_to_actual_theme_for_edit;?>
</button>
                                    <?php } else { ?>
                                        <div class="btn btn_small btn-block btn-outline-info disabled">
                                            <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'checked'), 0, true);
?>
                                            <span><?php echo $_smarty_tpl->tpl_vars['btr']->value->module_design_already_redefined_in_actual_theme;?>
</span>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </div>

                                <div class="okay_list_footer fn_action_block">
                    <div class="okay_list_foot_left">
                                                <div class="okay_list_heading okay_list_check">
                            <input class="hidden_check fn_check_all" type="checkbox" id="check_all_2" name="" value=""/>
                            <label class="okay_ckeckbox" for="check_all_2"></label>
                        </div>
                        <div class="okay_list_option">
                            <select name="action" class="selectpicker form-control">
                                <option value="clone_to_theme"><?php echo $_smarty_tpl->tpl_vars['btr']->value->module_design_copy_to_actual_theme_for_edit;?>
</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn_small btn_blue">
                        <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'checked'), 0, true);
?>
                        <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_apply, ENT_QUOTES, 'UTF-8', true);?>
</span>
                    </button>
                </div>
            </div>
        </form>
    <?php } else { ?>
        <div class="heading_box mt-1">
            <div class="text_grey"><?php echo $_smarty_tpl->tpl_vars['btr']->value->module_design_files_not_exists;?>
</div>
        </div>
    <?php }?>
</div>
<?php }
}
