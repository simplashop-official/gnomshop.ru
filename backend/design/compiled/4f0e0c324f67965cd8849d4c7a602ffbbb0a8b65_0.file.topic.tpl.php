<?php
/* Smarty version 3.1.36, created on 2020-06-12 21:33:59
  from '/home/gnomshop/public_html/backend/design/html/topic.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee3ca971ac2a5_18894593',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4f0e0c324f67965cd8849d4c7a602ffbbb0a8b65' => 
    array (
      0 => '/home/gnomshop/public_html/backend/design/html/topic.tpl',
      1 => 1589884281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:svg_icon.tpl' => 2,
    'file:pagination.tpl' => 1,
  ),
),false)) {
function content_5ee3ca971ac2a5_18894593 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['topic']->value->id) {?>
    <?php $_smarty_tpl->_assignInScope('meta_title', htmlspecialchars($_smarty_tpl->tpl_vars['topic']->value->header, ENT_QUOTES, 'UTF-8', true) ,false ,32);
} else { ?>
    <?php $_smarty_tpl->_assignInScope('meta_title', $_smarty_tpl->tpl_vars['btr']->value->topic_new ,false ,32);
}?>

<div class="row">
    <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
        <div class="wrap_heading">
            <div class="box_heading heading_page">
                <?php if ($_smarty_tpl->tpl_vars['topic']->value->id) {?>
                <?php echo $_smarty_tpl->tpl_vars['btr']->value->topic_number;?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['topic']->value->id, ENT_QUOTES, 'UTF-8', true);?>
 (<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'balance' ][ 0 ], array( $_smarty_tpl->tpl_vars['topic']->value->spent_time ));?>
) - <?php echo $_smarty_tpl->tpl_vars['comments_count']->value;?>

                <?php } else { ?>
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_new, ENT_QUOTES, 'UTF-8', true);?>

                <?php }?>
            </div>
            <div class="box_btn_heading">
                <a class="btn btn_small btn-info" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('controller'=>'SupportAdmin','id'=>null),$_smarty_tpl ) );?>
">
                    <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'return'), 0, false);
?>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_back, ENT_QUOTES, 'UTF-8', true);?>
</span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
        <div class="wrap_heading wrap_head_mob float-sm-right">
            <a class="btn btn_blue btn_small" target="_blank" href="https://okay-cms.com/support">
                <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'sertificat'), 0, true);
?>
                <span class="ml-q"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_condition, ENT_QUOTES, 'UTF-8', true);?>
</span>
            </a>
        </div>
    </div>
</div>


<?php if ($_smarty_tpl->tpl_vars['message_error']->value) {?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="alert alert--center alert--icon alert--error">
                <div class="alert__content">
                    <div class="alert__title">
                        <?php if ($_smarty_tpl->tpl_vars['message_error']->value == 'domain_not_found') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_no_domain, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'domain_disabled') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_domain_blocked, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'wrong_key') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_wrong_keys, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'topic_not_found') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_no_theme, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'topic_closed') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_closed, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'localhost') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_local, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'empty_comment') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_empty_comment, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_error']->value == 'empty_name') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_empty_name, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } else { ?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message_error']->value, ENT_QUOTES, 'UTF-8', true);?>

                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }?>


<form class="fn_form_list" method="post">
    <input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="boxed fn_toggle_wrap ">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_heading_accesses, ENT_QUOTES, 'UTF-8', true);?>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading_label">
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_attention_accesses, ENT_QUOTES, 'UTF-8', true);?>

                        </div>
                        <div class="mb-1">
                            <textarea class="form-control okay_textarea" name="accesses"><?php echo $_smarty_tpl->tpl_vars['accesses']->value;?>
</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="boxed">
                <div class="toggle_body_wrap on fn_card">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm 12">
                            <?php $_smarty_tpl->_subTemplateRender('file:pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <?php if ($_smarty_tpl->tpl_vars['comments']->value) {?>
                            <div class="okay_list">
                                                                <div class="okay_list_head">
                                    <div class="okay_list_heading okay_list_topic_name"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_name, ENT_QUOTES, 'UTF-8', true);?>
</div>
                                    <div class="okay_list_heading okay_list_topic_message"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_message, ENT_QUOTES, 'UTF-8', true);?>
</div>
                                    <div class="okay_list_heading okay_list_topic_time"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_spent_time, ENT_QUOTES, 'UTF-8', true);?>
</div>
                                </div>
                                                                <div class="okay_list_body">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['comments']->value, 'comment');
$_smarty_tpl->tpl_vars['comment']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['comment']->value) {
$_smarty_tpl->tpl_vars['comment']->do_else = false;
?>
                                    <div class="fn_row okay_list_body_item">
                                        <div class="okay_list_row">
                                            <div class="okay_list_boding okay_list_topic_name">
                                                <div class="text_dark text_600 mb-q mr-1 <?php if ($_smarty_tpl->tpl_vars['comment']->value->is_support) {?>text-primary<?php }?>">
                                                    <?php if ($_smarty_tpl->tpl_vars['comment']->value->is_support) {?>Support: <?php }?>
                                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['comment']->value->manager, ENT_QUOTES, 'UTF-8', true);?>

                                                </div>
                                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->support_last_answer, ENT_QUOTES, 'UTF-8', true);?>

                                                <?php if ($_smarty_tpl->tpl_vars['topic']->value->last_comment) {?>
                                                <span class="tag tag-default"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'date' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value->created ));?>
 <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'time' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value->created ));?>
</span>
                                                <?php }?>
                                            </div>

                                            <div class="okay_list_boding okay_list_topic_message">
                                                <?php echo $_smarty_tpl->tpl_vars['comment']->value->text;?>

                                            </div>

                                            <div class="okay_list_boding okay_list_topic_time <?php if ($_smarty_tpl->tpl_vars['comment']->value->spent_time < 0) {?>text-success<?php }?>">
                                                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'balance' ][ 0 ], array( $_smarty_tpl->tpl_vars['comment']->value->spent_time ));?>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </div>
                            </div>
                            <?php } else { ?>
                            <div class="heading_box mt-1">
                                <div class="text_grey">Нет сообщений</div>
                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['topic']->value->status != 'closed') {?>
    <div class="row">
        <div class="col-md-12">
            <div class="boxed match fn_toggle_wrap tabs">
                <div class="heading_box">
                    Написать сообщение
                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="icon-arrow-down"></i></a>
                    </div>
                </div>
                <div class="toggle_body_wrap on fn_card row">
                    <div class="col-md-12">
                        <?php if ($_smarty_tpl->tpl_vars['topic']->value->id) {?>
                            <div class="my-q mb-1">
                                <span class="text_700 text_primary font_16">
                                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['topic']->value->header, ENT_QUOTES, 'UTF-8', true);
if ($_smarty_tpl->tpl_vars['topic']->value->status == 'closed') {?> (Closed)<?php }?>
                                </span>
                                <span class="text_400 text_grey font_14">
                                    (<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'date' ][ 0 ], array( $_smarty_tpl->tpl_vars['topic']->value->created ));?>
 <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'time' ][ 0 ], array( $_smarty_tpl->tpl_vars['topic']->value->created ));?>
)
                                </span>
                            </div>
                        <?php } else { ?>
                            <div class="">
                                <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_new_theme, ENT_QUOTES, 'UTF-8', true);?>
</div>
                                <div class="mb-1">
                                    <input name="header" class="name form-control" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['topic_header']->value, ENT_QUOTES, 'UTF-8', true);?>
" type="text">
                                </div>
                            </div>
                        <?php }?>
                        <input name="id" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['topic']->value->id, ENT_QUOTES, 'UTF-8', true);?>
"/>
                    </div>
                    <div class="col-md-12">
                        <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_message, ENT_QUOTES, 'UTF-8', true);?>
</div>
                        <textarea name="comment_text" id="fn_editor" class="form-control okay_textarea editor_small"><?php echo nl2br($_smarty_tpl->tpl_vars['topic_message']->value);?>
</textarea>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 fn_action_block mt-1">
                        <?php if ($_smarty_tpl->tpl_vars['topic']->value->id) {?>
                        <button type="submit" class="btn btn_small btn-danger" name="close_topic" value="1">
                            <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_close, ENT_QUOTES, 'UTF-8', true);?>
</span>
                        </button>
                        <?php }?>
                        <button type="submit" class="btn btn_small btn_blue float-md-right" name="new_message" value="1">
                            <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->topic_send, ENT_QUOTES, 'UTF-8', true);?>
</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
</form>
<?php if ($_smarty_tpl->tpl_vars['subfolder']->value != '/') {?>
    <?php echo '<script'; ?>
 type="text/javascript" src="/<?php echo $_smarty_tpl->tpl_vars['subfolder']->value;?>
backend/design/js/tinymce_jq/tinymce.min.js"><?php echo '</script'; ?>
>
<?php } else { ?>
    <?php echo '<script'; ?>
 type="text/javascript" src="/design/js/tinymce_jq/tinymce.min.js"><?php echo '</script'; ?>
>
<?php }
echo '<script'; ?>
>
    $(function(){
        tinyMCE.init({
            selector: "textarea.editor_small",
            height: '300',
            plugins: [
                "advlist autolink lists link image preview anchor responsivefilemanager",
                "hr visualchars autosave noneditable searchreplace wordcount visualblocks",
                "code fullscreen save charmap nonbreaking",
                "insertdatetime media table paste imagetools"
            ],
            toolbar_items_size : 'small',
            menubar:'edit view format table',
            toolbar1: "fontselect formatselect fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | forecolor backcolor | table | link unlink | fullscreen visualblocks visualchars",
            statusbar: true,
            font_formats: "Andale Mono=andale mono,times;"+
            "Arial=arial,helvetica,sans-serif;"+
            "Arial Black=arial black,avant garde;"+
            "Book Antiqua=book antiqua,palatino;"+
            "Comic Sans MS=comic sans ms,sans-serif;"+
            "Courier New=courier new,courier;"+
            "Georgia=georgia,palatino;"+
            "Helvetica=helvetica;"+
            "Impact=impact,chicago;"+
            "Symbol=symbol;"+
            "Tahoma=tahoma,arial,helvetica,sans-serif;"+
            "Terminal=terminal,monaco;"+
            "Times New Roman=times new roman,times;"+
            "Trebuchet MS=trebuchet ms,geneva;"+
            "Verdana=verdana,geneva;"+
            "Webdings=webdings;"+
            "Wingdings=wingdings,zapf dingbats",


            save_enablewhendirty: true,
            save_title: "save",
            theme_advanced_buttons3_add : "save",
            save_onsavecallback: function() {
                $("[type='submit']").trigger("click");
                },

            language : "<?php echo $_smarty_tpl->tpl_vars['manager']->value->lang;?>
",
            /* Замена тега P на BR при разбивке на абзацы
             force_br_newlines : true,
             force_p_newlines : false,
             forced_root_block : '',
             */
            });
    });
<?php echo '</script'; ?>
>
<?php }
}
