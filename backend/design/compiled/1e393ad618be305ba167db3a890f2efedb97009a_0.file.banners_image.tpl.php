<?php
/* Smarty version 3.1.36, created on 2020-06-12 00:28:04
  from '/home/gnomshop/public_html/Okay/Modules/OkayCMS/Banners/Backend/design/html/banners_image.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee2a1e4d9c185_00326795',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1e393ad618be305ba167db3a890f2efedb97009a' => 
    array (
      0 => '/home/gnomshop/public_html/Okay/Modules/OkayCMS/Banners/Backend/design/html/banners_image.tpl',
      1 => 1591762669,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:svg_icon.tpl' => 5,
  ),
),false)) {
function content_5ee2a1e4d9c185_00326795 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['banners_image']->value->id) {?>
    <?php $_smarty_tpl->_assignInScope('meta_title', $_smarty_tpl->tpl_vars['banners_image']->value->name ,false ,32);
} else { ?>
    <?php $_smarty_tpl->_assignInScope('meta_title', $_smarty_tpl->tpl_vars['btr']->value->banners_image_add_banner ,false ,32);
}?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="wrap_heading">
            <div class="box_heading heading_page">
                <?php if (!$_smarty_tpl->tpl_vars['banners_image']->value->id) {?>
                     <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_add_banner, ENT_QUOTES, 'UTF-8', true);?>

                <?php } else { ?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->name, ENT_QUOTES, 'UTF-8', true);?>

                <?php }?>
            </div>
        </div>
    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['message_success']->value) {?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="alert alert--center alert--icon alert--success">
                <div class="alert__content">
                    <div class="alert__title">
                        <?php if ($_smarty_tpl->tpl_vars['message_success']->value == 'added') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_added, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } elseif ($_smarty_tpl->tpl_vars['message_success']->value == 'updated') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_updated, ENT_QUOTES, 'UTF-8', true);?>

                        <?php } else { ?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message_success']->value, ENT_QUOTES, 'UTF-8', true);?>

                        <?php }?>
                    </div>
                </div>
                <?php if ($_GET['return']) {?>
                <a class="alert__button" href="<?php echo $_GET['return'];?>
">
                    <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'return'), 0, false);
?>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_back, ENT_QUOTES, 'UTF-8', true);?>
</span>
                </a>
                <?php }?>
            </div>
        </div>
    </div>
<?php }?>

<form method="post" enctype="multipart/form-data" class="fn_fast_button">
    <input type=hidden name="session_id" value="<?php echo $_SESSION['id'];?>
">
    <input type="hidden" name="lang_id" value="<?php echo $_smarty_tpl->tpl_vars['lang_id']->value;?>
" />
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="boxed">
                <div class="row d_flex">
                                        <div class="col-lg-10 col-md-9 col-sm-12">
                        <div class="heading_label heading_label--required">
                            <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_name, ENT_QUOTES, 'UTF-8', true);?>
</span>
                            <i class="fn_tooltips" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->tooltip_banner_name, ENT_QUOTES, 'UTF-8', true);?>
">
                                <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'icon_tooltips'), 0, true);
?>
                            </i>
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="name" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->name, ENT_QUOTES, 'UTF-8', true);?>
"/>
                            <input name="id" type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->id, ENT_QUOTES, 'UTF-8', true);?>
"/>
                        </div>
                        <div class="row">
                            <div class=" col-lg-6 col-md-10">
                                <div class="heading_label heading_label--required" >
                                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_banner_group, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    <i class="fn_tooltips" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->tooltip_banner_group, ENT_QUOTES, 'UTF-8', true);?>
">
                                        <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'icon_tooltips'), 0, true);
?>
                                    </i>
                                </div>
                                <select name="banner_id" class="selectpicker form-control mb-1">
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['banners']->value, 'banner');
$_smarty_tpl->tpl_vars['banner']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['banner']->value) {
$_smarty_tpl->tpl_vars['banner']->do_else = false;
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['banner']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['banners_image']->value->banner_id == $_smarty_tpl->tpl_vars['banner']->value->id) {?>selected<?php } elseif (!$_smarty_tpl->tpl_vars['banners_image']->value->id && $_smarty_tpl->tpl_vars['banner_id']->value == $_smarty_tpl->tpl_vars['banner']->value->id) {?>selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                            <div class="col-lg-6 col-md-10">
                                <div class="heading_label">
                                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_url, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    <i class="fn_tooltips" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->tooltip_banner_url, ENT_QUOTES, 'UTF-8', true);?>
">
                                        <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'icon_tooltips'), 0, true);
?>
                                    </i>
                                </div>
                                <div class="form-group">
                                    <input name="url" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->url, ENT_QUOTES, 'UTF-8', true);?>
" />
                                </div>
                            </div>
                        </div>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_design_block'][0], array( array('block'=>"banner_image_brand_general"),$_smarty_tpl ) );?>

                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12">
                        <div class="activity_of_switch">
                            <div class="activity_of_switch_item">                                 <div class="okay_switch clearfix">
                                    <label class="switch_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_enable, ENT_QUOTES, 'UTF-8', true);?>
</label>
                                    <label class="switch switch-default">
                                        <input class="switch-input" name="visible" value='1' type="checkbox" id="visible_checkbox" <?php if ($_smarty_tpl->tpl_vars['banners_image']->value->visible || !$_smarty_tpl->tpl_vars['banners_image']->value->id) {?>checked=""<?php }?>/>
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_design_block'][0], array( array('block'=>"banner_image_switch_checkboxes"),$_smarty_tpl ) );?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-12 pr-0">
            <div class="boxed fn_toggle_wrap min_height_230px">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_image, ENT_QUOTES, 'UTF-8', true);?>

                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="fa fn_icon_arrow fa-angle-down"></i></a>
                    </div>
                </div>
                <div class="toggle_body_wrap fn_card on text-xs-center">
                    <input type="hidden" class="fn_accept_delete" name="delete_image" value="">
                    <div class="banner_image banner_image--small text-xs-center">
                        <?php if ($_smarty_tpl->tpl_vars['banners_image']->value->image) {?>
                            <a href="javascript:;" class="fn_delete_banner remove_image"></a>
                            <img class="admin_banner_images" src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'resize' ][ 0 ], array( $_smarty_tpl->tpl_vars['banners_image']->value->image,465,265,false,$_smarty_tpl->tpl_vars['config']->value->resized_banners_images_dir ));?>
" alt="" />
                        <?php }?>
                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_design_block'][0], array( array('block'=>"banner_image_image"),$_smarty_tpl ) );?>

                    </div>
                    <div class="fn_upload_image dropzone_block_image text-xs-center <?php if ($_smarty_tpl->tpl_vars['banners_image']->value->image) {?> hidden<?php }?>">
                        <i class="fa fa-plus font-5xl" aria-hidden="true"></i>
                        <input class="dropzone_banner" name="image" type="file" />
                    </div>
                </div>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_design_block'][0], array( array('block'=>"banner_image_image_block"),$_smarty_tpl ) );?>

            </div>
        </div>
        <div class="col-md-12 col-lg-4 pr-0">
            <div class="boxed fn_toggle_wrap min_height_230px">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_variant_show, ENT_QUOTES, 'UTF-8', true);?>

                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="fa fn_icon_arrow fa-angle-down"></i></a>
                    </div>
                </div>
                <div class="banner_type">
                    <div class="banner_type__item">
                        <input id="banner-type_1" class="hidden_check" name="settings[variant_show]" type="radio" <?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'])) && $_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'] == Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_DEFAULT || empty($_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'])) {?>checked<?php }?> value="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_DEFAULT;?>
" />
                        <label for="banner-type_1" class="okay_ckeckbox">
                            <span class="banner_type__img_wrap"></span>
                            <span class="banner_type__name_wrap"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_variant_1, ENT_QUOTES, 'UTF-8', true);?>
</span>
                        </label>
                    </div>
                    <div class="banner_type__item">
                        <input id="banner-type_2" class="hidden_check" name="settings[variant_show]" type="radio" <?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'])) && $_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'] == Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_DARK) {?>checked<?php }?> value="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_DARK;?>
" />
                        <label for="banner-type_2" class="okay_ckeckbox">
                            <span class="banner_type__img_wrap"></span>
                            <span class="banner_type__name_wrap"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_variant_2, ENT_QUOTES, 'UTF-8', true);?>
</span>
                        </label>
                    </div>
                    <div class="banner_type__item">
                        <input id="banner-type_3" class="hidden_check" name="settings[variant_show]" type="radio" <?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'])) && $_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'] == Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_IMAGE_LEFT) {?>checked<?php }?> value="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_IMAGE_LEFT;?>
" />
                        <label for="banner-type_3" class="okay_ckeckbox">
                            <span class="banner_type__img_wrap"></span>
                            <span class="banner_type__name_wrap"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_variant_3, ENT_QUOTES, 'UTF-8', true);?>
</span>
                        </label>
                    </div>
                    <div class="banner_type__item">
                        <input id="banner-type_4" class="hidden_check" name="settings[variant_show]" type="radio" <?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'])) && $_smarty_tpl->tpl_vars['banners_image']->value->settings['variant_show'] == Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_IMAGE_RIGHT) {?>checked<?php }?> value="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::SHOW_IMAGE_RIGHT;?>
" />
                        <label for="banner-type_4" class="okay_ckeckbox">
                            <span class="banner_type__img_wrap"></span>
                            <span class="banner_type__name_wrap"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_variant_4, ENT_QUOTES, 'UTF-8', true);?>
</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-4">
            <div class="boxed fn_toggle_wrap min_height_230px">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banner_resize_title, ENT_QUOTES, 'UTF-8', true);?>

                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="fa fn_icon_arrow fa-angle-down"></i></a>
                    </div>
                </div>
                <div class="mb-1">
                    <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_size_desktop, ENT_QUOTES, 'UTF-8', true);?>
</div>
                    <div class="banner_group__inputs mt-q">
                        <div class="banner_group__input">
                            <div class="input-group">
                                <input name="settings[desktop][w]" class="form-control" type="text" value="<?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['desktop']['w']))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->settings['desktop']['w'], ENT_QUOTES, 'UTF-8', true);
}?>" placeholder="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::DEFAULT_DESKTOP_W;?>
" />
                                <span class="input-group-addon">px</span>
                            </div>

                        </div>
                        <div class="banner_group__input">
                            <div class="input-group">
                                <input name="settings[desktop][h]" class="form-control" type="text" value="<?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['desktop']['h']))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->settings['desktop']['h'], ENT_QUOTES, 'UTF-8', true);
}?>" placeholder="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::DEFAULT_DESKTOP_H;?>
" />
                                <span class="input-group-addon">px</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="">
                    <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_size_mobile, ENT_QUOTES, 'UTF-8', true);?>
</div>
                    <div class="banner_group__inputs mt-q">
                        <div class="banner_group__input">
                            <div class="input-group">
                                <input name="settings[mobile][w]" class="form-control" type="text" value="<?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['mobile']['w']))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->settings['mobile']['w'], ENT_QUOTES, 'UTF-8', true);
}?>" placeholder="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::DEFAULT_MOBILE_W;?>
" />
                                <span class="input-group-addon">px</span>
                            </div>

                        </div>
                        <div class="banner_group__input">
                            <div class="input-group">
                                <input name="settings[mobile][h]" class="form-control" type="text" value="<?php if ((isset($_smarty_tpl->tpl_vars['banners_image']->value->settings['mobile']['h']))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->settings['mobile']['h'], ENT_QUOTES, 'UTF-8', true);
}?>" placeholder="<?php echo Okay\Modules\OkayCMS\Banners\Entities\BannersImagesEntity::DEFAULT_MOBILE_H;?>
" />
                                <span class="input-group-addon">px</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
                <div class="col-md-12">
            <div class="boxed fn_toggle_wrap min_height_230px">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_param, ENT_QUOTES, 'UTF-8', true);?>

                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="fa fn_icon_arrow fa-angle-down"></i></a>
                    </div>
                </div>
                <div class="toggle_body_wrap on fn_card">
                    <div class="row">
                        <div class="col-lg-6 pr-0">
                            <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_alt, ENT_QUOTES, 'UTF-8', true);?>
</div>
                            <div class="mb-1">
                                <input name="alt" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->alt, ENT_QUOTES, 'UTF-8', true);?>
" />
                            </div>
                            <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_title, ENT_QUOTES, 'UTF-8', true);?>
</div>
                            <div class="mb-1">
                                <input name="title" class="form-control" type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->title, ENT_QUOTES, 'UTF-8', true);?>
" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="heading_label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->banners_image_description, ENT_QUOTES, 'UTF-8', true);?>
</div>
                            <div class="mb-1">
                                <textarea name="description" class="form-control okay_textarea "><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banners_image']->value->description, ENT_QUOTES, 'UTF-8', true);?>
</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_design_block'][0], array( array('block'=>"banner_image_meta"),$_smarty_tpl ) );?>

            </div>
        </div>
    </div>

    <?php ob_start();
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['get_design_block'][0], array( array('block'=>"banner_image_custom_block"),$_smarty_tpl ) );
$_prefixVariable1 = ob_get_clean();
$_smarty_tpl->_assignInScope('block', $_prefixVariable1);?>
    <?php if (!empty($_smarty_tpl->tpl_vars['block']->value)) {?>
        <div class="row custom_block">
            <?php echo $_smarty_tpl->tpl_vars['block']->value;?>

        </div>
    <?php }?>

    <div class="row">
       <div class="col-lg-12 col-md-12 ">
            <button type="submit" class="btn btn_small btn_blue float-md-right">
                <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'checked'), 0, true);
?>
                <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_apply, ENT_QUOTES, 'UTF-8', true);?>
</span>
            </button>
        </div>
    </div>
</form>
<?php echo '<script'; ?>
>
    $(document).on("click", ".fn_delete_banner",function () {
       $(this).closest(".banner_image").find("img").remove();
       $(this).remove();
       $(".fn_upload_image ").removeClass("hidden");
        $(".fn_accept_delete").val(1);
    });

    if(window.File && window.FileReader && window.FileList) {

        $(".fn_upload_image").on('dragover', function (e){
            e.preventDefault();
            $(this).css('background', '#bababa');
        });
        $(".fn_upload_image").on('dragleave', function(){
            $(this).css('background', '#f8f8f8');
        });
        function handleFileSelect(evt){
            var files = evt.target.files; // FileList object
            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {
                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }
                var reader = new FileReader();
                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        // Render thumbnail.
                        $("<a href='javascript:;' class='fn_delete_banner remove_image'></a><img class='admin_banner_images' onerror='$(this).closest(\"div\").remove();' src='"+e.target.result+"' />").appendTo("div.banner_image ");
                        $(".fn_upload_image").addClass("hidden");
                    };
                })(f);
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }
            $(".fn_upload_image").removeAttr("style");
        }
        $(document).on('change','.dropzone_banner',handleFileSelect);
    }
<?php echo '</script'; ?>
>
<?php }
}
