<?php
/* Smarty version 3.1.36, created on 2020-06-16 18:55:04
  from '/home/gnomshop/public_html/Okay/Modules/OkayCMS/Hotline/Backend/design/html/hotline_xml.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee8eb585907b6_53917941',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '02d7044c6c92bb3e5f59d8ed1738f0481d6b668c' => 
    array (
      0 => '/home/gnomshop/public_html/Okay/Modules/OkayCMS/Hotline/Backend/design/html/hotline_xml.tpl',
      1 => 1589884280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:svg_icon.tpl' => 1,
  ),
),false)) {
function content_5ee8eb585907b6_53917941 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'category_select' => 
  array (
    'compiled_filepath' => '/home/gnomshop/public_html/backend/design/compiled/02d7044c6c92bb3e5f59d8ed1738f0481d6b668c_0.file.hotline_xml.tpl.php',
    'uid' => '02d7044c6c92bb3e5f59d8ed1738f0481d6b668c',
    'call_name' => 'smarty_template_function_category_select_3559420685ee8eb5848c517_80233356',
  ),
));
$_smarty_tpl->_assignInScope('meta_title', htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__title, ENT_QUOTES, 'UTF-8', true) ,false ,32);?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="wrap_heading">
            <div class="box_heading heading_page">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__title, ENT_QUOTES, 'UTF-8', true);?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="alert alert--icon alert--info">
            <div class="alert__content">
                <div class="alert__title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->alert_info, ENT_QUOTES, 'UTF-8', true);?>
</div>
                <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__generation_url, ENT_QUOTES, 'UTF-8', true);?>
 <a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url_generator'][0], array( array('route'=>'OkayCMS.Hotline.Feed','absolute'=>1),$_smarty_tpl ) );?>
" target="_blank"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url_generator'][0], array( array('route'=>'OkayCMS.Hotline.Feed','absolute'=>1),$_smarty_tpl ) );?>
</a></p>
            </div>
        </div>
    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['message_success']->value) {?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="alert alert--center alert--icon alert--success">
                <div class="alert__content">
                    <div class="alert__title">
                        <?php if ($_smarty_tpl->tpl_vars['message_success']->value == 'saved') {?>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_settings_saved, ENT_QUOTES, 'UTF-8', true);?>

                        <?php }?>
                    </div>
                </div>
                <?php if ($_GET['return']) {?>
                <a class="alert__button" href="<?php echo $_GET['return'];?>
">
                    <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'return'), 0, false);
?>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_back, ENT_QUOTES, 'UTF-8', true);?>
</span>
                </a>
                <?php }?>
            </div>
        </div>
    </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['message_error']->value) {?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="alert alert--center alert--icon alert--error">
            <div class="alert__content">
                <div class="alert__title">
                    <?php if ($_smarty_tpl->tpl_vars['message_error']->value == 'empty_name') {?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_enter_title, ENT_QUOTES, 'UTF-8', true);?>

                    <?php } else { ?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message_error']->value, ENT_QUOTES, 'UTF-8', true);?>

                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>

<form method="post" enctype="multipart/form-data" class="fn_fast_button fn_is_translit_alpha">
    <input type=hidden name="session_id" value="<?php echo $_SESSION['id'];?>
">
    <input type="hidden" name="lang_id" value="<?php echo $_smarty_tpl->tpl_vars['lang_id']->value;?>
" />

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="boxed fn_toggle_wrap">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__params, ENT_QUOTES, 'UTF-8', true);?>

                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="fa fn_icon_arrow fa-angle-down"></i></a>
                    </div>
                </div>
                <div class="toggle_body_wrap on fn_card">
                    <div class="permission_block">
                        <div class="permission_boxes row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="permission_box permission_box--long">
                                    <span class="permission_box__label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__upload_non_exists_products_to_hotline, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    <label class="switch switch-default">
                                        <input class="switch-input" name="okaycms__hotline__upload_only_available_to_hotline" value='1' type="checkbox" <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__upload_only_available_to_hotline) {?>checked=""<?php }?>/>
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="permission_box permission_box--long">
                                    <span class="permission_box__label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__store, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    <label class="switch switch-default">
                                        <input class="switch-input" name="okaycms__hotline__store" value='1' type="checkbox" <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__store) {?>checked=""<?php }?>/>
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="permission_box">
                                    <span class="permission_box__label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__use_full_description_to_hotline, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    <label class="switch switch-default">
                                        <input class="switch-input" name="okaycms__hotline__use_full_description_to_hotline" value='1' type="checkbox" <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__use_full_description_to_hotline) {?>checked=""<?php }?>/>
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="permission_box">
                                    <span class="permission_box__label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__no_export_without_price, ENT_QUOTES, 'UTF-8', true);?>
</span>
                                    <label class="switch switch-default">
                                        <input class="switch-input" name="okaycms__hotline__no_export_without_price" value='1' type="checkbox" <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__no_export_without_price) {?>checked=""<?php }?>/>
                                        <span class="switch-label"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-1">
                            <div class="heading_label">
                                <strong><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__company;?>
</strong>
                            </div>
                            <div class="mb-1">
                                <input class="form-control" type="text" name="okaycms__hotline__company" value="<?php echo $_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__company;?>
" />
                            </div>
                        </div>
                        <div class="col-md-6 mb-1">
                            <div class="heading_label">
                                <strong><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__guarantee_manufacturer;?>
</strong>
                            </div>
                            <div class="mb-1">
                                <select name="okaycms__hotline__guarantee_manufacturer" class="selectpicker">
                                    <option <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__guarantee_manufacturer == 0) {?>selected=""<?php }?> value=""></option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['features']->value, 'feature');
$_smarty_tpl->tpl_vars['feature']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['feature']->value) {
$_smarty_tpl->tpl_vars['feature']->do_else = false;
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__guarantee_manufacturer == $_smarty_tpl->tpl_vars['feature']->value->id) {?>selected=""<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['feature']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['feature']->value->name;?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 mb-1">
                            <div class="heading_label">
                                <?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__guarantee_shop;?>

                            </div>
                            <div class="mb-1">
                                <select name="okaycms__hotline__guarantee_shop" class="selectpicker">
                                    <option <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__guarantee_shop == 0) {?>selected=""<?php }?> value=""></option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['features']->value, 'feature');
$_smarty_tpl->tpl_vars['feature']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['feature']->value) {
$_smarty_tpl->tpl_vars['feature']->do_else = false;
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__guarantee_shop == $_smarty_tpl->tpl_vars['feature']->value->id) {?>selected=""<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['feature']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['feature']->value->name;?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 mb-1">
                            <div class="heading_label">
                                <strong><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__country_of_origin;?>
</strong>
                            </div>
                            <div class="mb-1">
                                <select name="okaycms__hotline__country_of_origin" class="selectpicker">
                                    <option <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__country_of_origin == 0) {?>selected=""<?php }?> value=""></option>
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['features']->value, 'feature');
$_smarty_tpl->tpl_vars['feature']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['feature']->value) {
$_smarty_tpl->tpl_vars['feature']->do_else = false;
?>
                                        <option <?php if ($_smarty_tpl->tpl_vars['settings']->value->okaycms__hotline__country_of_origin == $_smarty_tpl->tpl_vars['feature']->value->id) {?>selected=""<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['feature']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['feature']->value->name;?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12 ">
                            <button type="submit" class="btn btn_small btn_blue float-md-right">
                                <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_apply, ENT_QUOTES, 'UTF-8', true);?>
</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="boxed fn_toggle_wrap">
                <div class="heading_box">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__upload_products, ENT_QUOTES, 'UTF-8', true);?>

                    <div class="toggle_arrow_wrap fn_toggle_card text-primary">
                        <a class="btn-minimize" href="javascript:;" ><i class="fa fn_icon_arrow fa-angle-down"></i></a>
                    </div>
                </div>

                                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="boxed match fn_toggle_wrap">
                            <div class="heading_box">
                                <?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__categories;?>

                                <button class="btn btn_small btn-info" name="add_all_categories" value="1"><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__select_all;?>
</button>
                                <button class="btn btn_small" name="remove_all_categories" value="1"><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__select_none;?>
</button>
                            </div>
                            <div class="toggle_body_wrap on fn_card">
                                <select style="opacity: 0;" class="selectpicker_categories col-xs-12 px-0" multiple name="categories[]" size="10" data-selected-text-format="count" >
                                    
                                    <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'category_select', array('categories'=>$_smarty_tpl->tpl_vars['categories']->value), true);?>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6">
                        <div class="boxed match fn_toggle_wrap">
                            <div class="heading_box">
                                <?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__brands;?>

                                <button class="btn btn_small btn-info" name="add_all_brands" value="1"><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__select_all;?>
</button>
                                <button class="btn btn_small" name="remove_all_brands" value="1"><?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__select_none;?>
</button>
                            </div>
                            <div class="toggle_body_wrap on fn_card">
                                <select style="opacity: 0;" class="selectpicker_brands col-xs-12 px-0" multiple name="brands[]" size="10" data-selected-text-format="count" >
                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['brands']->value, 'brand');
$_smarty_tpl->tpl_vars['brand']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['brand']->value) {
$_smarty_tpl->tpl_vars['brand']->do_else = false;
?>
                                        <option value='<?php echo $_smarty_tpl->tpl_vars['brand']->value->id;?>
' class="brand_to_xml" <?php if ($_smarty_tpl->tpl_vars['brand']->value->to__okaycms__hotline) {?>selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['brand']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                    <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                </select>
                            </div>
                        </div>
                    </div>

                    
                        <?php echo '<script'; ?>
>
                            $('.selectpicker_categories').selectpicker();
                            $('.selectpicker_brands').selectpicker();
                        <?php echo '</script'; ?>
>
                    

                    <div class="col-lg-6 col-md-12">
                        <div class="boxed fn_toggle_wrap min_height_210px">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['backend_compact_product_list'][0], array( array('title'=>$_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__products_for_upload,'name'=>'related_products','products'=>$_smarty_tpl->tpl_vars['related_products']->value,'label'=>$_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__add_products,'placeholder'=>$_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__select_products),$_smarty_tpl ) );?>

                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="boxed fn_toggle_wrap min_height_210px">
                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['backend_compact_product_list'][0], array( array('title'=>$_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__products_not_for_upload,'name'=>'not_related_products','products'=>$_smarty_tpl->tpl_vars['not_related_products']->value,'label'=>$_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__add_products,'placeholder'=>$_smarty_tpl->tpl_vars['btr']->value->okaycms__hotline__select_products),$_smarty_tpl ) );?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 200px;">
        <div class="col-lg-12 col-md-12 ">
            <button type="submit" class="btn btn_small btn_blue float-md-right">
                <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_apply, ENT_QUOTES, 'UTF-8', true);?>
</span>
            </button>
        </div>
    </div>
</form>

<style>

    .permission_box__label {
        width: auto !important;
    }

</style><?php }
/* smarty_template_function_category_select_3559420685ee8eb5848c517_80233356 */
if (!function_exists('smarty_template_function_category_select_3559420685ee8eb5848c517_80233356')) {
function smarty_template_function_category_select_3559420685ee8eb5848c517_80233356(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('selected_id'=>$_smarty_tpl->tpl_vars['product_category']->value,'level'=>0), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['categories']->value, 'category');
$_smarty_tpl->tpl_vars['category']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->do_else = false;
?>
                                            <option value='<?php echo $_smarty_tpl->tpl_vars['category']->value->id;?>
' class="category_to_xml" <?php if ($_smarty_tpl->tpl_vars['category']->value->to__okaycms__hotline) {?>selected=""<?php }?>><?php
$__section_sp_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['level']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_sp_0_total = $__section_sp_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_sp'] = new Smarty_Variable(array());
if ($__section_sp_0_total !== 0) {
for ($__section_sp_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sp']->value['index'] = 0; $__section_sp_0_iteration <= $__section_sp_0_total; $__section_sp_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sp']->value['index']++){
?>&nbsp;&nbsp;&nbsp;&nbsp;<?php
}
}
echo $_smarty_tpl->tpl_vars['category']->value->name;?>
</option>
                                            <?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'category_select', array('categories'=>$_smarty_tpl->tpl_vars['category']->value->subcategories,'selected_id'=>$_smarty_tpl->tpl_vars['selected_id']->value,'level'=>$_smarty_tpl->tpl_vars['level']->value+1), true);?>

                                        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    <?php
}}
/*/ smarty_template_function_category_select_3559420685ee8eb5848c517_80233356 */
}
