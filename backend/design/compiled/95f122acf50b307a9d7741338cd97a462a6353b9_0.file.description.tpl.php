<?php
/* Smarty version 3.1.36, created on 2020-06-13 01:33:43
  from '/home/gnomshop/public_html/Okay/Modules/SimplaMarket/SimilarProducts/Backend/design/html/description.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee402c77d77e5_51331038',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '95f122acf50b307a9d7741338cd97a462a6353b9' => 
    array (
      0 => '/home/gnomshop/public_html/Okay/Modules/SimplaMarket/SimilarProducts/Backend/design/html/description.tpl',
      1 => 1590778292,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ee402c77d77e5_51331038 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('meta_title', htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->simplamarket_similar_products_title, ENT_QUOTES, 'UTF-8', true) ,false ,32);?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="wrap_heading">
            <div class="box_heading heading_page">
                <?php echo $_smarty_tpl->tpl_vars['btr']->value->simplamarket_similar_products_title;?>

            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12 float-xs-right"></div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="boxed">
            <div class="row d_flex">
                <div class="col-lg-12 col-md-12">
                    <?php echo $_smarty_tpl->tpl_vars['btr']->value->simplamarket_similar_products_description;?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
