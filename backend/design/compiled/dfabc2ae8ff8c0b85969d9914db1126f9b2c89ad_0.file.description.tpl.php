<?php
/* Smarty version 3.1.36, created on 2020-06-13 01:33:26
  from '/home/gnomshop/public_html/Okay/Modules/OkayCMS/Integration1C/Backend/design/html/description.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee402b6208f32_36096588',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dfabc2ae8ff8c0b85969d9914db1126f9b2c89ad' => 
    array (
      0 => '/home/gnomshop/public_html/Okay/Modules/OkayCMS/Integration1C/Backend/design/html/description.tpl',
      1 => 1589884280,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:svg_icon.tpl' => 1,
  ),
),false)) {
function content_5ee402b6208f32_36096588 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('meta_title', htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__integration_ic__description_title, ENT_QUOTES, 'UTF-8', true) ,false ,32);?>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="wrap_heading">
            <div class="box_heading heading_page">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->okaycms__integration_ic__description_title, ENT_QUOTES, 'UTF-8', true);?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="alert alert--icon alert--info">
            <div class="alert__content">
                <div class="alert__title mb-q"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->alert_info, ENT_QUOTES, 'UTF-8', true);?>
</div>
                <div class="text_box">
                    <p class="mb-1">
                        <?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__integration_ic__description_part_1;?>
 (<b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url_generator'][0], array( array('route'=>"integration_1c",'absolute'=>1),$_smarty_tpl ) );?>
</b>) <?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__integration_ic__description_part_2;?>
.
                    </p>
                    <p class="mb-1">
                        <?php echo $_smarty_tpl->tpl_vars['btr']->value->okaycms__integration_ic__description_part_3;?>
 <b>OkayCMS</b>: <a href="https://okay-cms.com/article/instruktsiya-po-nastrojke-obmena-dannymi-sajta-s-1s-8h-na-primere-konfiguratsii-ut-23" target="_blank">https://okay-cms.com/article/instruktsiya-po-nastrojke-obmena-dannymi-sajta-s-1s-8h-na-primere-konfiguratsii-ut-23</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
        <div class="col-lg-12 col-md-12 pr-0">
        <div class="boxed fn_toggle_wrap">
            <div class="toggle_body_wrap on fn_card">
                <form class="fn_form_list" method="post">
                    <input type="hidden" value="status" name="status">
                    <input type="hidden" name="session_id" value="<?php echo $_SESSION['id'];?>
">
                    <div class="okay_list">
                                                <div class="okay_list_head">
                            <div class="okay_list_heading okay_list_order_stg_sts_name_1c"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_name, ENT_QUOTES, 'UTF-8', true);?>
</div>
                            <div class="okay_list_heading okay_list_order_stg_sts_status2"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_action, ENT_QUOTES, 'UTF-8', true);?>
</div>
                            <div class="okay_list_heading okay_list_close"></div>
                        </div>
                        <div class="fn_status_list fn_sort_list okay_list_body sortable">
                            <?php if ($_smarty_tpl->tpl_vars['orders_statuses']->value) {?>
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['orders_statuses']->value, 'order_status');
$_smarty_tpl->tpl_vars['order_status']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['order_status']->value) {
$_smarty_tpl->tpl_vars['order_status']->do_else = false;
?>
                                    <div class="fn_row okay_list_body_item">
                                        <div class="okay_list_row fn_sort_item">
                                            <input type="hidden" name="id[]" value="<?php echo $_smarty_tpl->tpl_vars['order_status']->value->id;?>
">

                                            <div class="okay_list_boding okay_list_order_stg_sts_name_1c">
                                                <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_status']->value->name, ENT_QUOTES, 'UTF-8', true);?>
</span>

                                                <?php if ($_smarty_tpl->tpl_vars['is_mobile']->value == true) {?>
                                                    <div class="hidden-sm-up mt-q">
                                                        <select name="status_1c[<?php echo $_smarty_tpl->tpl_vars['order_status']->value->id;?>
]" class="selectpicker form-control">
                                                            <option value="not_use" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == '') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_action, ENT_QUOTES, 'UTF-8', true);?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_not_use, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                            <option value="new" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == 'new') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_action, ENT_QUOTES, 'UTF-8', true);?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_new, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                            <option value="accepted" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == 'accepted') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_action, ENT_QUOTES, 'UTF-8', true);?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_accepted, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                            <option value="to_delete" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == 'to_delete') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_action, ENT_QUOTES, 'UTF-8', true);?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_to_delete, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        </select>
                                                    </div>
                                                <?php }?>
                                            </div>

                                            <?php if ($_smarty_tpl->tpl_vars['is_mobile']->value == false) {?>
                                                <div class="okay_list_boding okay_list_order_stg_sts_status2">
                                                    <select name="status_1c[<?php echo $_smarty_tpl->tpl_vars['order_status']->value->id;?>
]" class="selectpicker form-control">
                                                        <option value="not_use" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == '') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_not_use, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="new" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == 'new') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_new, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="accepted" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == 'accepted') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_accepted, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="to_delete" <?php if ($_smarty_tpl->tpl_vars['order_status']->value->status_1c == 'to_delete') {?>selected=""<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_to_delete, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    </select>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            <?php }?>

                            <div class="fn_row fn_new_status fn_sort_item okay_list_body">
                                <div class="fn_row okay_list_body_item">
                                    <div class="okay_list_row fn_sort_item">
                                        <div class="okay_list_boding okay_list_drag"></div>
                                        <div class="okay_list_boding okay_list_order_stg_sts_name_1c">
                                            <input type="text" class="form-control" name="new_name[]" value="">
                                            <?php if ($_smarty_tpl->tpl_vars['is_mobile']->value == true) {?>
                                                <div class="hidden-sm-up mt-q">
                                                    <select name="new_is_close[]" class="selectpicker form-control">
                                                        <option value="1"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_reduse_products, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="0"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_not_reduse_products, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    </select>
                                                </div>
                                                <div class="hidden-sm-up mt-q">
                                                    <select name="new_status_1c[]" class="selectpicker form-control">
                                                        <option value="not_use"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_not_use, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="new"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_new, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="accepted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_accepted, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                        <option value="to_delete"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_to_delete, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    </select>
                                                </div>
                                            <?php }?>
                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['is_mobile']->value == false) {?>
                                            <div class="okay_list_boding okay_list_order_stg_sts_status">
                                                <select name="new_is_close[]" class="selectpicker form-control">
                                                    <option value="1"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_reduse_products, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <option value="0"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_not_reduse_products, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                </select>
                                            </div>
                                            <div class="okay_list_boding okay_list_order_stg_sts_status2">
                                                <select name="new_status_1c[]" class="selectpicker form-control">
                                                    <option value="not_use"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_not_use, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <option value="new"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_new, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <option value="accepted"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_accepted, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                    <option value="to_delete"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_1c_to_delete, ENT_QUOTES, 'UTF-8', true);?>
</option>
                                                </select>
                                            </div>
                                        <?php }?>
                                        <div class="okay_list_boding okay_list_order_stg_sts_label">
                                            <input name="new_color[]" value="" class="hidden">
                                            <span data-hint="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->order_settings_select_colour, ENT_QUOTES, 'UTF-8', true);?>
" class="fn_color label_color_item hint-bottom-middle-t-info-s-small-mobile  hint-anim"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                                                <div class="okay_list_footer">
                            <div class="okay_list_foot_left"></div>
                            <button type="submit" value="labels" class="btn btn_small btn_blue">
                                <?php $_smarty_tpl->_subTemplateRender('file:svg_icon.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('svgId'=>'checked'), 0, false);
?>
                                <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_apply, ENT_QUOTES, 'UTF-8', true);?>
</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


    <link rel="stylesheet" media="screen" type="text/css" href="design/js/colorpicker/css/colorpicker.css" />
    <?php echo '<script'; ?>
 type="text/javascript" src="design/js/colorpicker/js/colorpicker.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
        $(function() {
            var new_label = $(".fn_new_label").clone(true);
            $(".fn_new_label").remove();

            var new_status = $(".fn_new_status").clone(true);
            $(".fn_new_status").remove();

            $(document).on("click", ".fn_add_Label", function () {
                clone_label = new_label.clone(true);
                $(".fn_labels_list").append(clone_label);
            });

            $(document).on("click", ".fn_add_status", function () {
                clone_status = new_status.clone(true);
                clone_status.find("select").selectpicker();
                $(".fn_status_list").append(clone_status);
            });

            $(document).on("mouseenter click", ".fn_color", function () {
                var elem = $(this);
                elem.ColorPicker({
                    onChange: function (hsb, hex, rgb) {
                        elem.css('backgroundColor', '#' + hex);
                        elem.prev().val(hex);
                    }
                });
            });

        });
    <?php echo '</script'; ?>
>


<?php }
}
