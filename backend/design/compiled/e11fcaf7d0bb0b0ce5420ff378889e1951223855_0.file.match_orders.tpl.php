<?php
/* Smarty version 3.1.36, created on 2020-06-13 16:44:58
  from '/home/gnomshop/public_html/backend/design/html/match_orders.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5ee4d85ae8ecb9_67061240',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e11fcaf7d0bb0b0ce5420ff378889e1951223855' => 
    array (
      0 => '/home/gnomshop/public_html/backend/design/html/match_orders.tpl',
      1 => 1589884281,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:pagination.tpl' => 1,
  ),
),false)) {
function content_5ee4d85ae8ecb9_67061240 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="match_order">
    <div class="match_order__head">
        <div class="match_order__cell match_order__cell--id">
            <?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_id;?>

        </div>
        <div class="match_order__cell match_order__cell--name">
            <?php echo $_smarty_tpl->tpl_vars['btr']->value->general_name;?>

        </div>
        <div class="match_order__cell match_order__cell--identify match_order__cell--center hidden-sm-down">
            <?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by;?>

        </div>
        <div class="match_order__cell  match_order__cell--status match_order__cell--center hidden-sm-down">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->general_status, ENT_QUOTES, 'UTF-8', true);?>

        </div>
        <div class="match_order__cell  match_order__cell--prices match_order__cell--center hidden-xs-down">
            <?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_total_price;?>

        </div>
    </div>
    <div class="match_order__body">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['match_orders']->value, 'match_order');
$_smarty_tpl->tpl_vars['match_order']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['match_order']->value) {
$_smarty_tpl->tpl_vars['match_order']->do_else = false;
?>
        <div class="match_order__row">
            <div class="match_order__cell match_order__cell--id">
                <a class="text_600 mb-h" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('controller'=>'OrderAdmin','id'=>$_smarty_tpl->tpl_vars['match_order']->value->id),$_smarty_tpl ) );?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['btr']->value->orders_order, ENT_QUOTES, 'UTF-8', true);?>
 #<?php echo $_smarty_tpl->tpl_vars['match_order']->value->id;?>
</a>
                <div class="hidden-md-up">
                    <?php if ($_smarty_tpl->tpl_vars['match_order']->value->match_by_email && $_smarty_tpl->tpl_vars['match_order']->value->match_by_phone) {?>
                    <div class="tag tag-ind_email"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_email;?>
</div>
                    <div class="tag tag-ind_phone"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_phone;?>
</div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['match_order']->value->match_by_email) {?>
                    <div class="tag tag-ind_email"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_email;?>
</div>
                    <?php } elseif ($_smarty_tpl->tpl_vars['match_order']->value->match_by_phone) {?>
                    <div class="tag tag-ind_phone"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_phone;?>
</div>
                    <?php } else { ?>
                    <div class="tag tag-ind_unknown"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_unknown;?>
</div>
                    <?php }?>
                </div>
            </div>
            <div class="match_order__cell match_order__cell--name">
                <div class="text_400 mb-h"><?php echo $_smarty_tpl->tpl_vars['match_order']->value->name;?>
</div>
                <div class="font_12 text_600 text_grey mb-h"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'date' ][ 0 ], array( $_smarty_tpl->tpl_vars['match_order']->value->date ));?>
|<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'time' ][ 0 ], array( $_smarty_tpl->tpl_vars['match_order']->value->date ));?>
</div>
                <div class="hidden-sm-up"><div class="tag tag--np text_700 mb-h" style="color: #<?php echo $_smarty_tpl->tpl_vars['order']->value->status_color;?>
;"><?php echo $_smarty_tpl->tpl_vars['order']->value->status_id;?>
</div></div>

                <div class="input-group input-group--small hidden-sm-up">
                    <span class="form-control">
                        <?php echo $_smarty_tpl->tpl_vars['match_order']->value->total_price;?>

                    </span>
                    <span class="input-group-addon">
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->sign, ENT_QUOTES, 'UTF-8', true);?>

                    </span>
                </div>
            </div>
            <div class="match_order__cell match_order__cell--identify match_order__cell--center hidden-sm-down">
                <?php if ($_smarty_tpl->tpl_vars['match_order']->value->match_by_email && $_smarty_tpl->tpl_vars['match_order']->value->match_by_phone) {?>
                    <div class="tag tag-ind_email"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_email;?>
</div>
                    <div class="tag tag-ind_phone"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_phone;?>
</div>
                <?php } elseif ($_smarty_tpl->tpl_vars['match_order']->value->match_by_email) {?>
                    <div class="tag tag-ind_email"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_email;?>
</div>
                <?php } elseif ($_smarty_tpl->tpl_vars['match_order']->value->match_by_phone) {?>
                    <div class="tag tag-ind_phone"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_phone;?>
</div>
                <?php } else { ?>
                    <div class="tag tag-ind_unknown"><?php echo $_smarty_tpl->tpl_vars['btr']->value->order_match_by_unknown;?>
</div>
                <?php }?>
            </div>
            <div class="match_order__cell  match_order__cell--status match_order__cell--center hidden-sm-down">
                <div class="tag--np tag text_700" style="color: #<?php echo $_smarty_tpl->tpl_vars['order']->value->status_color;?>
;"><?php echo $_smarty_tpl->tpl_vars['match_order']->value->status_name;?>
</div>
            </div>
            <div class="match_order__cell match_order__cell--prices match_order__cell--center hidden-xs-down">
                <div class="input-group input-group--small">
                    <span class="form-control">
                        <?php echo $_smarty_tpl->tpl_vars['match_order']->value->total_price;?>

                    </span>
                    <span class="input-group-addon">
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value->code, ENT_QUOTES, 'UTF-8', true);?>

                    </span>
                </div>
            </div>
        </div>
        <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['current_page']->value !== 'all') {?>
        <?php $_smarty_tpl->_subTemplateRender('file:pagination.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    <?php }?>
</div>


<?php echo '<script'; ?>
>

    $('.tab_navigation_link').on('mouseup', function() {
        const matchOrdersTab = $(this).hasClass('fn_match_orders_tab_title');

        if (matchOrdersTab && ! hasMatchOrdersTabParam(window.location.href)) {
            const url = addQueryParam(window.location.href, 'match_orders_tab_active', 1);
            window.history.pushState('', '', url);
            return;
        }

        if (!matchOrdersTab && hasMatchOrdersTabParam(window.location.href)) {
            const url = removeQueryParam(window.location.href, 'match_orders_tab_active');
            window.history.pushState('', '', url);
        }
    });

    $('.page-item a').on('click', function(e) {
        e.preventDefault();
        const link = $(this).attr('href');

        let target = '';
        if (! hasMatchOrdersTabParam(link) && hasMatchOrdersTabParam(window.location.href)) {
            target = addQueryParam(link, 'match_orders_tab_active', 1);
        } else if(hasMatchOrdersTabParam(link) && ! hasMatchOrdersTabParam(window.location.href)) {
            target = removeQueryParam(link, 'match_orders_tab_active');
        } else {
            target = link;
        }
        window.location = target;
    });

    function addQueryParam(url, paramName, paramValue) {
        return `${url}&${paramName}=${paramValue}`;
    }

    function removeQueryParam(url, paramName) {
        return url.split('&')
                  .map(item => item.split('='))
                  .filter(item => item[0] !== paramName)
                  .map(item => item.join('='))
                  .join('&');
    }

    function hasMatchOrdersTabParam(url) {
        return url.substr(window.location.href.indexOf('?') + 1)
                  .split('&')
                  .map(item => item.split('='))
                  .reduce((acc, item) => item[0] === 'match_orders_tab_active' ? true : acc, false);
    }

<?php echo '</script'; ?>
>


<style>
    .match-order {
        display: flex;
        flex-wrap: wrap;
        align-items: normal;
    }
    .match-order__field {
        width: 25%;
        padding: 5px;
    }
</style><?php }
}
