<?php

namespace Okay\Modules\OkCMS\ImportWithImages\Init;

use Okay\Admin\Helpers\BackendImportHelper;
use Okay\Core\Modules\AbstractInit;
use Okay\Modules\OkCMS\ImportWithImages\Extenders\BackendExtender;

class Init extends AbstractInit
{
    /**
     * @inheritDoc
     */
    public function install()
    {
        // install
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->registerChainExtension(
            ['class' => BackendImportHelper::class, 'method' => 'importImages'],
            ['class' => BackendExtender::class, 'method' => 'downloadImages']
        );
    }
}