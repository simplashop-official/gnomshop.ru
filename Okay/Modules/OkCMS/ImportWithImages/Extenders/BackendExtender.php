<?php

namespace Okay\Modules\OkCMS\ImportWithImages\Extenders;

use Okay\Core\Design;
use Okay\Core\EntityFactory;
use Okay\Core\Image;
use Okay\Core\Modules\Extender\ExtensionInterface;
use Okay\Core\Modules\Module;
use Okay\Core\QueryFactory;
use Okay\Core\Request;
use Okay\Core\ServiceLocator;

class BackendExtender implements ExtensionInterface
{
    private $request;
    private $entityFactory;
    private $design;
    private $module;
    private $image;
    private $queryBuilder;

    public function __construct(Request $request, EntityFactory $entityFactory, Design $design, Module $module, Image $image, QueryFactory $queryBuilder)
    {
        $this->request = $request;
        $this->entityFactory = $entityFactory;
        $this->design = $design;
        $this->module = $module;
        $this->image = $image;
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * Загружаем изображения после их обработки
     * @param $image_ids
     * @return mixed
     */
    public function downloadImages($image_ids)
    {
        foreach ($image_ids as $image_id) {
            $sql = $this->queryBuilder->newSqlQuery()->setStatement("SELECT * FROM __images WHERE id = :id AND filename LIKE 'http%'")->bindValues(['id' => $image_id])->execute();
            $result = $sql->result();

            if ($result) {
                if (!$originFile = $this->image->downloadImage(rawurldecode($result->filename))) {
                    $this->queryBuilder->newSqlQuery()->setStatement("DELETE FROM __images WHERE id = :id")->bindValues(['id' => $image_id])->execute();
                }
            }
        }

        return $image_ids;
    }
}