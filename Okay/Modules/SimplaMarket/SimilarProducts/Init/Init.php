<?php


namespace Okay\Modules\SimplaMarket\SimilarProducts\Init;


use Okay\Core\Modules\AbstractInit;
use Okay\Entities\ProductsEntity;
use Okay\Modules\SimplaMarket\SimilarProducts\Extensions\SimilarProductsExtension;

class Init extends AbstractInit
{
    public function install()
    {
        $this->setBackendMainController('DescriptionAdmin');
    }

    public function init()
    {
        $this->registerBackendController('DescriptionAdmin');
        $this->addBackendControllerPermission('DescriptionAdmin', 'products');

        $this->registerChainExtension(
            ['class' => ProductsEntity::class,           'method' => 'getRelatedProducts'],
            ['class' => SimilarProductsExtension::class, 'method' => 'similarProducts']);
    }
}