<?php


use Okay\Core\Design;
use Okay\Core\QueryFactory;
use Okay\Core\OkayContainer\Reference\ServiceReference as SR;
use Okay\Modules\SimplaMarket\SimilarProducts\Extensions\SimilarProductsExtension;


return [
    SimilarProductsExtension::class => [
        'class' => SimilarProductsExtension::class,
        'arguments' => [
            new SR(Design::class),
            new SR(QueryFactory::class),
        ],
    ],
];
