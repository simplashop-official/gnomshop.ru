<?php

$lang['simplamarket_similar_products_title'] = 'Связанные товары из категории';
$lang['simplamarket_similar_products_description'] = 'Данный модуль позволяет выводить в список связанных товаров товары из главной категории обозреваемого товара в случае если у него не указаны явно связанные с ним товары.';