<?php

$lang['simplamarket_similar_products_title'] = "დაკავშირებული პროდუქტები კატეგორიიდან";
$lang['simplamarket_similar_products_description'] = "ეს მოდული საშუალებას გაძლევთ მონიშნული პროდუქტის ძირითადი კატეგორიის პროდუქტების ჩამონათვალში აჩვენოთ, თუ იგი პირდაპირ არ მიუთითებს მასთან დაკავშირებულ პროდუქტებზე.";