<?php

$lang['simplamarket_similar_products_title'] = "Related products from the category";
$lang['simplamarket_similar_products_description'] = "This module allows you to display in the list of related products goods from the main category of the monitored product if it does not explicitly indicate products related to it.";