<?php


namespace Okay\Modules\SimplaMarket\SimilarProducts\Extensions;


use Okay\Core\Design;
use Okay\Core\QueryFactory;
use Okay\Entities\ProductsEntity;
use Okay\Core\Modules\Extender\ExtensionInterface;

class SimilarProductsExtension implements ExtensionInterface
{
    /**
     * @var Design
     */
    private $design;

    /**
     * @var QueryFactory
     */
    private $queryFactory;

    public function __construct(Design $design, QueryFactory $queryFactory)
    {
        $this->design       = $design;
        $this->queryFactory = $queryFactory;
    }

    public function similarProducts($result, $filter = [])
    {
        $controller  = $this->design->getVar('controller');
        $productsIds = $filter['product_id'];
        if (! empty($result) || $controller != 'ProductController') {
            return $result;
        }

        if (empty($productsIds)) {
            return [];
        }

        $sql = $this->queryFactory->newSqlQuery();
        $sql->setStatement("SELECT 
                p.id  product_id, 
                p2.id related_id, 
                p2.id position 
            FROM ". ProductsEntity::getTable() ." p 
            INNER JOIN ". ProductsEntity::getTable() ." p2 ON p.main_category_id=p2.main_category_id 
            WHERE p.id IN (:product_ids) 
              AND p.id != p2.id
            ORDER BY RAND()
            LIMIT 5 
        ")->bindValue('product_ids', $productsIds);
        return $sql->results();
    }
}